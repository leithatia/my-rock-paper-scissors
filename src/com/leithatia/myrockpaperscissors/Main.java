package com.leithatia.myrockpaperscissors;

public class Main {
    public static void main(String[] args) {
        Player player1 = new Player("Jimmy");
        Player player2 = new Player("Tom");

        Game game = new Game();
        game.play(player1, player2);
    }
}
