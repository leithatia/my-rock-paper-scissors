package com.leithatia.myrockpaperscissors;

public enum Move {
    ROCK("rock"),
    PAPER("paper"),
    SCISSORS("scissors");

    private String description;

    Move(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}


