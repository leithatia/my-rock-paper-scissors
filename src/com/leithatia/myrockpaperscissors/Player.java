package com.leithatia.myrockpaperscissors;

public class Player {

    // FIELDS

    private String name;
    private int score = 0;
    private Move currentMove;

    // CONSTRUCTORS

    Player(String name) {
        this.name = name;
    }

    // METHODS

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void setCurrentMove(Move move) {
        currentMove = move;
    }
    public Move getCurrentMove() {
        return currentMove;
    }

    public void incrementScore() {
        score++;
    }
}
