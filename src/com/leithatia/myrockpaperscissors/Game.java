package com.leithatia.myrockpaperscissors;

public class Game {
    // PROPERTIES
    private boolean gameOver = false;

    // METHODS

    // Start the game
    public void play(Player player1, Player player2) {
        while (!gameOver) {
            takeTurn(player1);
            takeTurn(player2);

            compareMoves(player1, player2);
            displayTurn(player1, player2);

            if (player1.getScore() >= 2 || player2.getScore() >= 2) {
                gameOver = true;
            }
        }

        System.out.printf("\n\n%s won!", player1.getScore() >= 2 ? player1.getName() : player2.getName());
    }

    // Method to choose a move for a player
    private void takeTurn(Player player) {
        int randomIndex = (int) (Math.random() * 3);
        Move[] moves = Move.values();
        player.setCurrentMove(moves[randomIndex]);
    }

    // Method to compare moves and increment a player's score if suitable
    private void compareMoves(Player player1, Player player2) {
        Move player1Move = player1.getCurrentMove();
        Move player2Move = player2.getCurrentMove();

        if (player1Move == player2Move) {
            return;
        } else if ((player1Move == Move.ROCK && player2Move == Move.SCISSORS) ||
                (player1Move == Move.PAPER && player2Move == Move.ROCK) ||
                (player1Move == Move.SCISSORS && player2Move == Move.PAPER)) {
            player1.incrementScore();
        } else {
            player2.incrementScore();
        }
    }

    // Method to display who played what and the outcome
    private void displayTurn(Player player1, Player player2) {
        System.out.printf("%s played %s and %s played %s\n", player1.getName(), player1.getCurrentMove(), player2.getName(), player2.getCurrentMove());
    }
}
